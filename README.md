# Frontend Mentor - Stats preview card component solution

Simple design of the Stats preview card component (Exercise of [Frontend Mentor](https://www.frontendmentor.io/challenges/stats-preview-card-component-8JqbgoU62)

## Welcome! 👋

Thanks for checking out this front-end coding challenge. It's a first simple design to improve my skills as a front-end developer.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Simple design of the Stats adaptative card component.

### Links

- Solution URL: [Add solution URL here](https://your-solution-url.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first design
- [Bulma](https://bulma.io/) - CSS library

### What I learned

Use this section to recap over some of your major learnings while working through this project. Writing these out and providing code samples of areas you want to highlight is a great way to reinforce your own knowledge.

To see how you can add code snippets, see below:

* Basics of bulma.io css framework
* Basics of flex to create a layout and make it adaptive to mobile and desktop
* Basic design and UI
* How to publish web site on gitlab (https://getpublii.com/docs/host-static-website-gitlab-pages.html)

```css
.card-horizontal {
  flex-direction: row-reverse;
}
```
Also
```css
.img-overlay:before {
      content: "";
      position: absolute;
      background: hsla(277, 81%, 43%, 0.41);
```
## Author

- Website - [Me on gitlab](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments
 
> Giving thanks with joy to the Father who has made you able to share the lot of God's holy people and with them to inherit the light. -Colossians 1:12

